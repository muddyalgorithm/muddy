import java.io.*;
import java.util.Scanner;
import java.util.*;

//원본 평문을 저장시켜 놓고 블럭으로 잘라서 반환/암호문 저장 후 반환 가능
public class TextBlock extends BinaryDigit{

    private String _plainData;
    private StringBuilder _binaryData;
    private ArrayList _divideData;

    private ArrayList<byte[]> _cypherData;
    private ArrayList<byte[]> _decypherData;
    private static int _blockIndex;
    private int _blockSize = 128;

    private String extension;

    public TextBlock()
    {
        _blockIndex = 0;
        Scanner scan = new Scanner(System.in);
        _plainData = scan.nextLine();

        _divideData = new ArrayList<String>();

        if(_plainData.length() > _blockSize/2)
        {
            int endLength = _plainData.length()-1;
            for(int i = 0; i < endLength; i += _blockSize/2)
            {
                if(i+(_blockSize/2-1) < endLength)
                    _divideData.add(_plainData.substring(i,i+_blockSize/2));
                else
                    _divideData.add(_plainData.substring(i,endLength+1));
            }
        }
        else
            _divideData.add(_plainData);
        _cypherData = new ArrayList<>();
        _decypherData = new ArrayList<>();
    }

    public TextBlock(String filePath)
    {
        //FileInputStream fis = null;
        BufferedInputStream fis = null;
        int count = 0;
        int fileSize = 0;
        try
        {
            //fis = new FileInputStream(filePath);
            fis = new BufferedInputStream(new FileInputStream(filePath));
            File f = new File(filePath);
            extension = filePath.split("\\.")[1];

            int readData = -1;

            _cypherData = new ArrayList<>();
            _decypherData = new ArrayList<>();
            _divideData = new ArrayList();

            fileSize = (int)f.length();

            byte[] bytes;

            if(fileSize > _blockSize)
            {
                int index = 0;
                bytes = new byte[_blockSize];
                while((readData = fis.read()) != -1)
                {
                    bytes[count] = (byte)readData;
                    count++;

                    if(count > _blockSize-1)
                    {
                        _divideData.add(index,bytes);
                        index++;
                        bytes = new byte[_blockSize];
                        count = 0;
                    }
                }
                _divideData.add(index,bytes);
            }
            else
            {
                bytes = new byte[fileSize];
                while((readData = fis.read()) != -1)
                {
                    bytes[count] = (byte)readData;
                    count++;
                }
                _divideData.add(bytes);
            }

        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            try{fis.close(); } catch(IOException e) { System.out.println(e); }
        }
    }

    public TextBlock(boolean value)
    {
        _cypherData = new ArrayList<>();
        _decypherData = new ArrayList<>();
        _divideData = new ArrayList();

    }
    public StringBuilder get_binaryData()
    {
        _binaryData = new StringBuilder();
        if(_divideData.get(_blockIndex).getClass() == String.class)
            _binaryData = StringToBinary((String)_divideData.get(_blockIndex));
        else
            _binaryData = StringToBinary((byte[])_divideData.get(_blockIndex));
        _blockIndex = (_blockIndex+1)%_divideData.size();
        return _binaryData;
    }

    public StringBuilder get_cypherData()
    {
        StringBuilder cypherData = ByteToBinary(_cypherData.get(_blockIndex));
        //_blockIndex = (_blockIndex+1)%_divideData.size();
        _blockIndex = (_blockIndex+1)%_cypherData.size();
        return cypherData;
    }

    public boolean testModule()
    {
        int index = _divideData.size();
        for(int i = 0; i < index; i++)
        {
            BitSet plain;
            if(_divideData.get(i).getClass() == String.class)
                plain = makeBit(StringToBinary((String)_divideData.get(i)));
            else
                plain = makeBit(StringToBinary((byte[])_divideData.get(i)));
            BitSet decypher = makeBit(ByteToBinary(_decypherData.get(i)));
            plain.xor(decypher);
            if (plain.isEmpty())
            {
                continue;
            }
            else
                return false;
        }
        return true;
    }

    public void outSerializedFile()
    {
        FileOutputStream fos = null;
        //BufferedOutputStream fos = null;
        ObjectOutputStream oos = null;

        try
        {
            if(extension == null)
                //fos = new BufferedOutputStream(new FileOutputStream("cypherData.dat"));
                fos = new FileOutputStream("cypherData.dat");
            else
                //fos = new BufferedOutputStream(new FileOutputStream("cypherData."+extension));
                fos = new FileOutputStream("cypherData."+extension);
            oos = new ObjectOutputStream(fos);

            oos.writeObject(_cypherData);
            //oos.flush();
            System.out.println("저장이 완료되었습니다.");
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(fos != null) try{fos.close();} catch(IOException e) { System.out.println(e); }
            if(oos != null) try{oos.close();} catch(IOException e) { System.out.println(e); }
        }
    }
    public void inSerializedFile(String filePath)
    {
        FileInputStream fis = null;
        ObjectInputStream ois = null;

        try
        {
            fis = new FileInputStream(filePath);
            ois = new ObjectInputStream(fis);
            extension = filePath.split("\\.")[1];
            _cypherData = (ArrayList<byte[]>)ois.readObject();
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(fis != null) try{fis.close();}catch(IOException e){System.out.println(e);}
            if(ois != null) try{ois.close();}catch(IOException e){System.out.println(e);}
        }
    }

    public void set_cypherData(StringBuilder cypher)
    {
        byte[] bytes = StringToByte(cypher);
        _cypherData.add(bytes);
    }
    public void set_decypherData(StringBuilder decypher)
    {
        byte[] bytes = StringToByte(decypher);
        _decypherData.add(bytes);
    }

    public void outDecypherFile()
    {
        FileOutputStream fos = null;
        BufferedWriter bw = null;
        try
        {
            if( !extension.matches("dat"))
            {
                fos = new FileOutputStream("originalData."+extension);
                for(byte[] bytes : _decypherData)
                {
                    fos.write(bytes);
                }
            }

            else
            {
                bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("originalData.txt")));
                int count = 0;

                for(byte[] bytes : _decypherData)
                {
                    byte[] Char16 = new byte[2];
                    for(byte b : bytes)
                    {
                        Char16[count] = b;
                        if(count == 1)
                        {
                            bw.write(new String(Char16,"UTF-16LE"));
                            Char16 = new byte[2];
                            count = 0;
                        }
                        else
                            count++;
                    }
                }
            }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            try{if(fos != null) fos.close();}catch(IOException e){ System.out.println(e);}
            try{if(bw != null) bw.close();}catch(IOException e){System.out.println(e);}
        }
    }
    public int get_MaxblockIndex() { return _divideData.size(); }
    public int get_CypherBlockIndex() { return _cypherData.size(); }
}
