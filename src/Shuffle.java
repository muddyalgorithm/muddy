import java.util.ArrayList;

public class Shuffle extends BinaryDigit {
    private int _addedKey = 0;

    public Shuffle(String keys) {
        int length = keys.length();
        for(int i = 0; i < length; ++i) {
            this._addedKey += keys.charAt(i);
        }

    }

    private int getSuffleNumber(int bitLength) {
        int shuffleNumber = (int)((Math.pow((double)this._addedKey, 3.0D) - Math.pow((double)this._addedKey, 2.0D)) % (double)bitLength / 4.0D + (double)(bitLength / 4));
        return shuffleNumber;
    }

    public StringBuilder Encyption(ArrayList<StringBuilder> data) {
        for(int listSize = data.size(); listSize != 1; listSize /= 2) {
            for(int i = 0; i < listSize; i += 2) {
                data.add(this.Merge((StringBuilder)data.get(0), (StringBuilder)data.get(1)));
                data.remove(0);
                data.remove(0);
            }
        }

        return (StringBuilder)data.get(0);
    }

    private StringBuilder Merge(StringBuilder leftData, StringBuilder rightData) {
        int shuffleNumber = this.getSuffleNumber(leftData.length() + rightData.length());
        this.MergeShuffle(shuffleNumber, leftData, rightData);
        this.MergeShuffle(shuffleNumber, rightData, leftData);
        this.MergeShuffle(shuffleNumber, rightData, leftData);
        rightData.append(leftData);
        return rightData;
    }

    private void MergeShuffle(int shuffleNumber, StringBuilder leftData, StringBuilder rightData) {
        StringBuilder tmp = new StringBuilder();
        int startLength = leftData.length() - shuffleNumber;

        int i;
        for(i = 0; i < shuffleNumber; ++i) {
            tmp.append(leftData.charAt(startLength + i));
            tmp.append(rightData.charAt(i));
        }

        for(i = 0; i < shuffleNumber; ++i) {
            leftData.setCharAt(startLength + i, tmp.charAt(i));
            rightData.setCharAt(i, tmp.charAt(shuffleNumber + i));
        }

    }

    public ArrayList<StringBuilder> Decyption(StringBuilder data) {
        ArrayList<StringBuilder> divideData = new ArrayList();
        divideData.add(data);
        int listSize = 1;

        for(int bitLength = ((StringBuilder)divideData.get(0)).length(); bitLength % 2 == 0; bitLength /= 2) {
            for(int i = 0; i < listSize; ++i) {
                this.Devide((StringBuilder)divideData.get(0), divideData);
                divideData.remove(0);
            }

            listSize = 2 * listSize;
        }

        return divideData;
    }

    private void Devide(StringBuilder data, ArrayList<StringBuilder> divideData) {
        int shuffleNumber = this.getSuffleNumber(data.length());
        StringBuilder leftData = new StringBuilder(data.substring(0, data.length() / 2));
        StringBuilder rightData = new StringBuilder(data.substring(data.length() / 2, data.length()));
        this.DevideShuffle(shuffleNumber, leftData, rightData);
        this.DevideShuffle(shuffleNumber, leftData, rightData);
        this.DevideShuffle(shuffleNumber, rightData, leftData);
        divideData.add(rightData);
        divideData.add(leftData);
    }

    private void DevideShuffle(int shuffleNumber, StringBuilder leftData, StringBuilder rightData) {
        StringBuilder tmp = new StringBuilder();
        int startLength = leftData.length() - shuffleNumber;

        int i;
        for(i = 0; i < 2 * shuffleNumber; ++i) {
            if (i < shuffleNumber) {
                tmp.append(leftData.charAt(startLength + i));
            } else {
                tmp.append(rightData.charAt(i - shuffleNumber));
            }
        }

        for(i = 0; i < shuffleNumber; ++i) {
            leftData.setCharAt(startLength + i, tmp.charAt(i * 2));
            rightData.setCharAt(i, tmp.charAt(i * 2 + 1));
        }

    }
}