import java.io.UnsupportedEncodingException;
import java.util.BitSet;

//입력되는 String 객체들을 0과 1로 변환시켜 저장, BitSet으로 변화시켜 XOR연산 가능하게 해줌
public class BinaryDigit {

    //문자를 0 또는 1로 이루어진 비트형식으로 바꾼다.
    protected StringBuilder StringToBinary(String data)
    {
        StringBuilder binary = new StringBuilder();
        try {
            byte[] bytes = data.getBytes("UTF-16LE");
            for (byte b : bytes) {
                int val = b;
                for (int i = 0; i < 8; i++) {
                    binary.append((val & 128) == 0 ? 0 : 1);
                    val <<= 1;
                }
            }
        }
        catch(UnsupportedEncodingException e)
        {
            System.out.println(e.toString());
        }
        return binary;
    }

    protected StringBuilder StringToBinary(byte[] data)
    {
        StringBuilder binary = new StringBuilder();
        byte[] bytes = data;
        for (byte b : bytes)
        {
            int val = b;
            for (int i = 0; i < 8; i++)
            {
                binary.append((val & 128) == 0 ? 0 : 1);
                val <<= 1;
            }
        }
        return binary;
    }

    protected StringBuilder ByteToBinary(byte[] bytes)
    {
        StringBuilder binary = new StringBuilder();
        for(byte b : bytes)
        {
            int val = b;
            for(int i = 0; i < 8; i++)
            {
                binary.append((val&128) == 0? 0 : 1);
                val<<=1;
            }
        }
        return binary;
    }

    //문자(0 또는 1로 이루어진)를 바이트 배열로 바꾸어 반환한다.
    protected byte[] StringToByte(StringBuilder data)
    {
        int count = data.length()/8;
        byte[] bytes = new byte[count];
        for(int index = 0; index < count; index++)
        {
            byte value = 0;
            byte binary = 1;
            for(int i = 7 + (8*index); i >= 0 + (8*index); i--)
            {
                if(data.charAt(i) == '1')
                    value += binary;
                binary*=2;
            }
            bytes[index] = value;
            //System.out.print(value+" ");
        }
        return bytes;
    }

    //0또는 1로 이루어진 문자열을 입력받아 BitSet 형식으로 바꾸어 반환한다.
    protected BitSet makeBit(StringBuilder binary)
    {
        BitSet bitData = new BitSet(binary.length());
        int length = binary.length();
        for(int i = 0; i < length; i++)
        {
            if(binary.charAt(i) == '1')
                bitData.set(i);
        }
        return bitData;
    }
}