import java.util.Arrays;
import java.util.Scanner;
import java.util.*;

public class KeyGenerator extends BinaryDigit{

    public static KeyGenerator _instance;
    private String _defaultKey;
    public String _newKey;
    private static int _count;
    private int _primKey;
    private ArrayList<String> _keyTable;

    private KeyGenerator()
    {
        _count = 0;
        if(_defaultKey == null)
        {
            Scanner scan = new Scanner(System.in);
            _defaultKey = scan.nextLine();
            _primKey = keyLength();
            _keyTable = new ArrayList<String>();
        }
    }

    //KeyGenerator 객체를 여러 개 생성하면 안되기 때문에 단 하나의 객체만 존재하도록 설정. 생성자를 private로 선언했기에 다른 클래스에서 호출 불가함
    public static KeyGenerator getInstance()
    {
        if(_instance == null)
            _instance = new KeyGenerator();
        return _instance;
    }
    //입력된 키의 길이보다 작은 최대의 소수 구함
    private int keyLength()
    {
        int prim = 2;
        int length = _defaultKey.length();
        for(int i = 2; i < length; i++)
        {
            for(int j = 2; j < i; j++)
            {
                if(i % j == 0)
                    break;
                else if(j == i-1)
                    prim = i;
                else
                    continue;
            }
        }
        return prim;
    }
    //입력된 키를 바탕으로 새로운 키를 생성하는 알고리즘, 전치 암호기법 사용
    private String makeKey()
    {
        int index = _count % _defaultKey.length();
        int charIndex = 0;

        String key;
        if(_count == 0)
            key = _defaultKey;
        else
            key = _newKey;

        boolean[] checkTable = new boolean[_defaultKey.length()];
        Arrays.fill(checkTable,Boolean.FALSE);

        char[] keyArray = new char[_defaultKey.length()];
        while(!isAllTrue(checkTable))
        {
            //해당되는 키 값을 이미 변경한 적이 있는지 체크. 변경되었던 값이라면 한 칸 증가시켜서 반복
            if(checkTable[index] == true)
            {
                index = (index + 1) % key.length();
                continue;
            }
            keyArray[charIndex] = key.charAt(index);
            charIndex++;
            checkTable[index] = true;
            index = (index + _primKey) % key.length();
        }
        _count++;
        _newKey = String.valueOf(keyArray);
        return _newKey;
    }

    //모든 값이 변경되었는지 체크
    private boolean isAllTrue(boolean[] array)
    {
        for(int i = 0; i < array.length; i++)
        {
            if(array[i])
                continue;
            else
                return false;
        }
        return true;
    }

    private StringBuilder translateKey(String keyData)
    {
        StringBuilder binaryKey = StringToBinary(keyData);
        StringBuilder inverseKey = StringToBinary(keyData).reverse();
        BitSet bitKey = makeBit(binaryKey);
        bitKey.xor(makeBit(inverseKey));

        int length = binaryKey.length();
        for(int i = 0; i < length; i++)
        {
            if(bitKey.get(i))
                binaryKey.setCharAt(i,'1');
            else
                binaryKey.setCharAt(i,'0');
        }
        return  binaryKey;
    }
    //새로운 키를 생성하여 0과 1로 값을 바꿔서 반환한다.
    public StringBuilder getKey()
    {
        makeKey();
        return  translateKey(_newKey);
    }
    //입력된 깊이 만큼의 키를 순차적으로 생성하여 리스트에 저장한다.
    public void makeKeyTable(int level)
    {
        //_count = 0;
        int count = 1;
        for(int i = 0; i < level; i++)
            count = count*2;
        count = count-2;

        for(int i = 0; i < count; i++)
        {
            _keyTable.add(makeKey());
        }
    }
    public void keyCountInit() { _count = 0; }
    //키 테이블에서 값을 역순으로 꺼낸다. 역순인 이유는 복호화 과정이 역순으로 진행되기 때문.
    public StringBuilder getKeyTable()
    {
        String keyData = _keyTable.get(_keyTable.size()-1);
        _keyTable.remove(_keyTable.size()-1);

        return  translateKey(keyData);
    }
    //생성된 키가 아닌 입력했던 기존의 키를 반환한다.
    public String get_defaultKey() { return _defaultKey; }
}
