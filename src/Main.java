import java.util.ArrayList;
import java.util.BitSet;
import java.util.*;

public class Main {


    public static void main(String[] args) {
        TextBlock block;
        String path;
        Scanner scan = new Scanner(System.in);
        KeyGenerator key;
        Shuffle shuffle;
        ExclusiveOR xor;
        int size;

        System.out.println("모드를 선택하세요(1. 암호화    2. 복호화)");
        try {
            int menu = scan.nextInt();
            switch (menu) {
                case 1:
                    System.out.println("1. 직접 입력    2. 파일 입력");
                    menu = scan.nextInt();
                    switch (menu) {
                        case 1:
                            block = new TextBlock();

                            System.out.println("키 입력");
                            key = KeyGenerator.getInstance();
                            shuffle = new Shuffle(key.get_defaultKey());

                            xor = new ExclusiveOR();
                            size = block.get_MaxblockIndex();
                            long startTime = System.nanoTime();
                            long endTime;
                            for (int i = 0; i < size; i++) {
                                ArrayList<StringBuilder> cypher1 = xor.Encyption(block.get_binaryData());
                                //System.out.println(cypher1);
                                StringBuilder cypher2 = shuffle.Encyption(cypher1);
                                //System.out.println(cypher2);
                                block.set_cypherData(cypher2);
                                xor.divideDataInit();
                            }
                            endTime = System.nanoTime();
                            System.out.println("처리 시간: " + (endTime - startTime));
                            block.outSerializedFile();
                            break;
                        case 2:
                            System.out.println("파일 경로를 입력하세요 ");
                            path = scan.next();
                            block = new TextBlock(path);

                            System.out.println("키 입력");
                            key = KeyGenerator.getInstance();
                            shuffle = new Shuffle(key.get_defaultKey());

                            xor = new ExclusiveOR();
                            size = block.get_MaxblockIndex();
                            startTime = System.nanoTime();
                            for (int i = 0; i < size; i++) {
                                ArrayList<StringBuilder> cypher1 = xor.Encyption(block.get_binaryData());
                                //System.out.println(cypher1);
                                StringBuilder cypher2 = shuffle.Encyption(cypher1);
                                //System.out.println(cypher2);
                                block.set_cypherData(cypher2);
                                xor.divideDataInit();
                            }
                            block.outSerializedFile();
                            endTime = System.nanoTime();
                            System.out.println("처리 시간: " + (endTime - startTime));
                            break;
                        default:
                            System.out.println("입력값이 잘못되었습니다.");
                            break;
                    }
                    break;
                case 2:
                    block = new TextBlock(true);
                    System.out.println("파일 경로를 입력하세요 ");
                    path = scan.next();
                    block.inSerializedFile(path);

                    System.out.println("키 입력");
                    key = KeyGenerator.getInstance();
                    shuffle = new Shuffle(key.get_defaultKey());

                    xor = new ExclusiveOR();

                    long startTime = System.nanoTime();
                    size = block.get_CypherBlockIndex();
                    for (int i = 0; i < size; i++) {
                        ArrayList<StringBuilder> decypher1 = shuffle.Decyption(block.get_cypherData());
                        //System.out.println(decypher1);
                        StringBuilder decypher2 = xor.Decryption(decypher1);
                        //System.out.println(decypher2);
                        block.set_decypherData(decypher2);
                    }
                    block.outDecypherFile();
                    break;
                default:
                    System.out.println("입력값이 잘못되었습니다.");
                    break;
            }
        }
        catch(Exception e)
        {
            System.out.println("main: "+e);
        }
    }
}