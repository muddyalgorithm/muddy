import java.util.*;

public class ExclusiveOR extends BinaryDigit{

    private KeyGenerator _keyGenerator;
    private ArrayList<StringBuilder> _divideData;

    public ExclusiveOR()
    {
        _keyGenerator = KeyGenerator.getInstance();
        _divideData = new ArrayList<StringBuilder>();
    }

    private void XORAlgorithm(StringBuilder data)
    {
        StringBuilder key = _keyGenerator.getKey();
        if(data.length() > key.length())
        {
            int count = data.length() - key.length();
            for(int i = 0; i < count; i++)
                key.append(key.charAt(i%key.length()));
        }
        else if(data.length() < key.length())
        {
            int count = key.length() - data.length();
            for(int i = 0; i < count; i++)
                key.deleteCharAt(key.length()-1);
        }
        //System.out.println("data: "+data+" key: "+key);
        BitSet bitData = makeBit(data);
        BitSet bitKey = makeBit(key);
        bitData.xor(bitKey);

        //bitData를 StringBuilder로 변환시켜야 함
        int length = data.length();
        for(int i = 0; i < length; i++)
        {
            if(bitData.get(i))
                data.setCharAt(i,'1');
            else
                data.setCharAt(i,'0');
        }
        DivideData(data);
    }

    //외부에서 호출 가능한 암호화 메소드. 먼저 분리하여 저장한 후 데이터 정보가 홀수가 될 때까지 각각의 블럭에 대해 XOR알고리즘 수행하고 제거한다.
    public ArrayList<StringBuilder> Encyption(StringBuilder plainData)
    {

        DivideData(plainData);

        while(_divideData.get(0).length()%2 == 0)
        {
            XORAlgorithm(_divideData.get(0));
            XORAlgorithm(_divideData.get(1));

            _divideData.remove(0);
            _divideData.remove(0);
        }
        return _divideData;
    }

    //복호화 단계에서 필요한 깊이 계산 알고리즘. 항상 완전이진트리를 이루기 때문에 가능하다.
    private int CalculateLevel(ArrayList<StringBuilder> data)
    {
        int level = data.size();
        int count = 0;

        while(level != 1)
        {
            level = level/2;
            count++;
        }
        return count;
    }

    //외부에서 호출 가능한 복호화 알고리즘. 앞에서부터 2개의 블록을 하나로 합쳐서 저장시킨 후 원본을 지우는 작업을 끝까지 반복한다. 이후 XOR알고리즘으로 복호화를 수행한다. 전체 리스트의 노드가 하나만 남을 때까지 반복한다.
    public StringBuilder Decryption(ArrayList<StringBuilder> encryptionData)
    {
        int level = CalculateLevel(encryptionData);
        _keyGenerator.makeKeyTable(level);

        for(int i = 0; i < level; i++)
        {
            int count = encryptionData.size()/2;
            for(int j = 0; j < count; j++)
            {
                StringBuilder newData = new StringBuilder();

                newData.append(encryptionData.get(0));
                newData.append(encryptionData.get(1));
                encryptionData.add(newData);

                encryptionData.remove(0);
                encryptionData.remove(0);
            }
            if(encryptionData.size() == 1)
                break;
            else
            {
                int listSize = encryptionData.size();
                for(int listCount = 0; listCount < listSize; listCount++)
                    DeXORAlgorithm(encryptionData.get(listSize-1-listCount));
                //역으로 XOR알고리즘 수행
            }
        }
        return encryptionData.get(0);
    }

    //전체적으로 XOR알고리즘과 동일하지만 마지막에 나누는 과정이 사라졌고, KEY값을 키 테이블에서 가져오는 점이 다르다.
    private void DeXORAlgorithm(StringBuilder data)
    {
        StringBuilder key = _keyGenerator.getKeyTable();
        if(data.length() > key.length())
        {
            int count = data.length() - key.length();
            for(int i = 0; i < count; i++)
                key.append(key.charAt(i%key.length()));
        }
        else if(data.length() < key.length())
        {
            int count = key.length() - data.length();
            for(int i = 0; i < count; i++)
                key.deleteCharAt(key.length()-1);
        }

        BitSet bitData = makeBit(data);
        BitSet bitKey = makeBit(key);
        bitData.xor(bitKey);

        //bitData를 StringBuilder로 변환시켜야 함
        int length = data.length();
        for(int i = 0; i < length; i++)
        {
            if(bitData.get(i))
                data.setCharAt(i,'1');
            else
                data.setCharAt(i,'0');
        }
    }
    //데이터 값을 나눠서 각각 저장시키는 메소드다.
    private void DivideData(StringBuilder plainData)
    {
        StringBuilder leftString = new StringBuilder(plainData.length()/2);
        StringBuilder rightString = new StringBuilder(plainData.length()/2);

        int length = plainData.length();
        for(int i = 0; i < length; i++)
        {
            if(i < length/2)
                leftString.append(plainData.charAt(i));
            else
                rightString.append((plainData.charAt(i)));
        }
        _divideData.add(leftString);
        _divideData.add(rightString);
    }
    public void divideDataInit()
    {
        _divideData.clear();
    }
}
